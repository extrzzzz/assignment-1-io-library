section .text
%define EXIT 60
%define NULL_TERMINATOR 0
%define SYS_CALL 1
%define STDOUT 1
%define NEW_STRING 0xA
%define SPACE 0x20
%define TAB 0x9
%define MIN 0x30
%define MAX 0x39


 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi+rax], NULL_TERMINATOR
        je .end
        inc rax
        jmp .loop
    .end:
        ret
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rax, SYS_CALL
    mov rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    mov rax, SYS_CALL
    mov rdx, 1
    push rdi
    mov rdi, STDOUT
    mov rsi, rsp
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_STRING
    jmp print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, rsp
    mov r10, 10
    dec rdi
    push 0
    sub rsp, 16
    .loop:
        xor rdx, rdx
        div r10
        add rdx, '0'
        dec rdi
        mov byte[rdi], dl
        test rax, rax
        jnz .loop
        call print_string
        add rsp, 24
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0
        jnl print_uint
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
        mov dl, byte[rdi+rax]
        cmp dl, byte[rsi+rax]
        jne .false
        test dl, dl
        je .true
        inc rax
        jmp .loop
    .false:
        xor rax, rax
        ret
    .true:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
   xor rcx, rcx
.loop:
    push rdi
    push rsi
    push rcx
    call read_char
    pop rcx
    pop rsi 
    pop rdi
    test rax, rax
    je .end
    cmp rax, NEW_STRING
    je .badsymbol
    cmp rax, SPACE
    je .badsymbol
    cmp rax, TAB
    je .badsymbol
    mov [rdi+rcx], rax
    inc rcx
    cmp rcx, rsi 
    jge .error 
    jmp .loop
.badsymbol:
    test rcx, rcx
    je .loop
    jmp .end
.error:
    xor rax, rax
    xor rdx, rdx
    ret
.end:
    xor rax, rax
    mov [rdi+rcx], rax
    mov rax, rdi
    mov rdx, rcx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r10, 10
    xor r11, r11
    .loop:
        mov al, byte[rdi+rcx]
        cmp al, '0'
        jb .end
        cmp al, '9'
        ja .end
        sub al, '0'
        push rax

        mov rax, r11
        mul r10
        mov r11, rax

        pop rax
        add r11, rax
        inc rcx
        jmp .loop
    .end:
        mov rdx, rcx
        mov rax, r11
        ret    



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r8, rdi
    cmp byte[r8], '-'
    je .negative
    jmp .positive
    .positive:
        mov rdi, r8
        jmp parse_uint
    .negative:
        inc r8
        mov rdi, r8
        call parse_uint
        neg rax
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    inc rax
    cmp rdx, rax
    jb .end
    xor rax, rax
    .loop:
        mov dl, byte[rdi+rax]
        mov byte[rsi+rax], dl
        inc rax
        test dl, dl
        jne .loop
    .end:
        xor rax, rax
        ret
